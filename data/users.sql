/*
 Navicat Premium Data Transfer

 Source Server         : homestead
 Source Server Type    : MySQL
 Source Server Version : 50709
 Source Host           : 192.168.10.10
 Source Database       : wxbdb

 Target Server Type    : MySQL
 Target Server Version : 50709
 File Encoding         : utf-8

 Date: 11/01/2016 14:39:48 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `createtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `users`
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES ('8', 'hahah', null, '2016-10-12 06:10:09'), ('9', 'hahah', null, '2016-10-12 06:10:25'), ('10', 'test', null, '2016-10-12 07:08:04'), ('12', 'phpunit', null, '2016-10-12 07:09:03'), ('13', 'phpunit', null, '2016-10-12 07:10:44'), ('14', 'phpunit', null, '2016-10-12 07:11:28'), ('15', 'phpunit', null, '2016-10-12 07:12:12'), ('16', 'phpunit', null, '2016-10-12 07:13:46'), ('17', 'phpunit', null, '2016-10-12 07:14:59'), ('18', 'phpunit', null, '2016-10-12 07:15:09'), ('19', 'phpunit', null, '2016-10-12 07:15:43'), ('20', 'phpunit', null, '2016-10-12 07:16:09'), ('21', '', '$2y$10$gYpWr2ZWdc.S.FSqcJZcJOSqvBoJdpSKeUkS1IOsEdQUQUxBlF6ra', '2016-10-13 03:36:51'), ('22', '', '$2y$10$CHS/kztKWesCT633Bhm34.O.vHS31mlwHD9M0/HDrTVTe5XxH5wBm', '2016-10-13 03:52:51'), ('24', 'zhangsan', '$2y$10$9aN4E2RCezZwSEZgYZtx2.o9JpxVBo5kOO35uQvn5l16dYlGIJBIq', '2016-11-01 06:27:35'), ('25', 'zhangsan', '$2y$10$HGP.KzfLoKIgon6cSGIMXeBYPsXHk//raalM3ob8tggR/9a9cGDsa', '2016-11-01 06:27:48'), ('26', 'zhangsan', '$2y$10$KNdzx9Hm7eTerqqesSIKH.NpBSmBZ5m5dvxX.NARRWkJDVRg/0k5e', '2016-11-01 06:28:31'), ('27', 'phpunit', null, '2016-11-01 06:35:37'), ('28', 'phpunit', null, '2016-11-01 06:35:57'), ('29', 'phpunit', null, '2016-11-01 06:36:24'), ('30', 'phpunit', null, '2016-11-01 06:36:54'), ('31', 'phpunit', null, '2016-11-01 06:37:22'), ('32', 'phpunit', null, '2016-11-01 06:37:43');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
